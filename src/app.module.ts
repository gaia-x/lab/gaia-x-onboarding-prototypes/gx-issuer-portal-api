import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MailsModule } from './mails/mails.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CredentialsModule } from './credentials/credentials.module'
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.${process.env.NODE_ENV}.env`],
      cache: true,
      isGlobal: true
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      name: 'RegistrationConnection',
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('POSTGRES_REGISTRATION_HOST'),
        port: configService.get('POSTGRES_REGISTRATION_PORT'),
        username: configService.get('POSTGRES_REGISTRATION_USER'),
        password: configService.get('POSTGRES_REGISTRATION_PASSWORD'),
        database: configService.get('POSTGRES_REGISTRATION_DB'),
        autoLoadEntities: true
      })
    }),
    MailsModule,
    CredentialsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule { }
