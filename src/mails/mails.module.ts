import { Module } from '@nestjs/common'
import { APP_GUARD } from '@nestjs/core'

import { MailsService } from './services/mails.service'
import { SendMailService } from './services/send-mail.service'

import { MailsController } from './mails.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { registration_record } from './entities/registration.entity'

import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler'
@Module({
  imports: [
    TypeOrmModule.forFeature([registration_record], 'RegistrationConnection'),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 60
    })],
  controllers: [MailsController],
  providers: [MailsService, SendMailService, {
    provide: APP_GUARD,
    useClass: ThrottlerGuard
  }],
  exports: [MailsService]
})
export class MailsModule { }
