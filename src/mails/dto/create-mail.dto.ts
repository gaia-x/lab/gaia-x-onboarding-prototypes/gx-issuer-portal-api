import { SessionIdRequest } from '../../common/dto/SessionIdRequest.dto'

export class CreateMailDto extends SessionIdRequest {
  email: string
}
