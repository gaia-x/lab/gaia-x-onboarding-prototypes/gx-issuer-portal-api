import { BadRequestException, PipeTransform } from '@nestjs/common'
import Joi from 'joi'
import { IssueCredentialsDto } from '../dto/IssueCredential.dto'
import { IssueCredentialRequestDto } from '../dto/IssueCredentialRequest.dto'

export const issueCredentialRequestSchema = Joi.object({
    credentialSubject: Joi.object({
        id: Joi.string().required().empty(),
        hasLegallyBindingName: Joi.string().required().empty(),
        hasRegistrationNumber: Joi.string().required().empty(),
        hasCountry: Joi.string().required().empty(),
        hasJurisdiction: Joi.string().required().empty(),
        leiCode: Joi.string().optional().empty(),
        parentOrganisation: Joi.string().optional().empty(),
        subOrganisation: Joi.string().optional().empty(),
        ethereumAddress: Joi.string().optional().empty()
    }).required().empty(),
    onboardingToken: Joi.string().required().empty(),
    walletId: Joi.string().optional().empty(),
    sessionId: Joi.string().optional().empty()
}).options({ abortEarly: false, allowUnknown: false })


export class IssueCredentialValidatorPipe implements PipeTransform<IssueCredentialRequestDto, IssueCredentialsDto> {
    public transform(query: IssueCredentialRequestDto): IssueCredentialsDto {

        const validate = issueCredentialRequestSchema.validate(query)

        if (validate.error) {
            const errorMessages = validate.error.details.map(d => d.message).join()
            throw new BadRequestException(errorMessages)
        }

        const validIssueCredentialRequest: IssueCredentialRequestDto = validate.value
        const { credentialSubject } = validIssueCredentialRequest

        return {
            credentials: [{
                credentialData: { credentialSubject },
                schemaId: 'https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json',
                type: 'ParticipantCredential'
            }],
            params: {
                walletId: validIssueCredentialRequest.walletId || 'walt.id',
                sessionId: validIssueCredentialRequest.sessionId
            },
            onboardingToken: validIssueCredentialRequest.onboardingToken
        }
    }
}
